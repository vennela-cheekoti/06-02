Ecommerce 06-02 
##overview
our application consists of 4 resourses (customer, product, order, orderlineitem)
When we click on customer on the navigation bar we will able to see the customer details.
When we click on product on the navigation bar we will able to see the product details.
when we click on order on the navigation bar we will able to see the order details.
When we click on orderlineitem on the navigation bar we will able to see the orderlineitem details.
In the home page we perform CURD operations for Customers, products, order and orderline item.
when we click on Aboutus on the navigation bar we will see the developer names, if we click on particular developer it will navigate to their personal site
For customer, product, order and orderlineitem we have separate controller, model and view files.

This application makes use of:
- Node.js platform
- Express web framework
- EJS templating engine
- MVC design pattern
- Mongoose MongoDB object modeling
- Lodash for JavaScript object iteration and manipulation 
- jQuery library for DOM manipulation
- BootStrap framework for responsive design
- nedb In-memory database
- Winston logger

## Code Editor

-  Install Visual Studio Code.
- Right-click on your project folder and select "Open with Code".
- RECOMMENDED: Under VS Code menu "File" option, check "Autosave".
- OPTIONAL: To type commands from within VS Code, from the VS Code menu, select View /  Terminal.

## Review Code Organization

- app.js - Starting point for the application. Defines the express server, requires routes and models. Loads everything and begins listening for events.
- config/ - configuration information configuration/environment variables
- controllers/ - logic for handling client requests
- data/ - seed data loaded each time the application starts
- models/ - schema descriptions for custom data types
- routes/ - route definitions for the API
- views/ - EJS - embedded JavaScript and HTML used to create dynamic pages

## Install Project Dependencies

Run npm install to install the project dependencies listed in package.json.

> npm install

## Run the App Locally

In your project folder, right-click and "Open Command Window Here as Administrator". At the prompt, type nodemon app.js to start the server.  (CTRL-C to stop.)

> node app.js

## View Web App

Open browser to the location displayed, e.g. http://localhost:8089/

#Team Members
- Vennela Cheekoti (product)
- Sathwika Gone (customer)
- Vishal Pannala (order)
- Saikumar Nalivela (orderlineitem)