/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Sathwika Gone <S533623@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  email: {
    type: String,
    required: true,
    unique: true
  },
  firstname: {
    type: String,
    required: true,
    default: 'First Name'
  },
  lastname: {
    type: String,
    required: true,
    default: 'Last Name'
  },
  phonenumber:{
    type: Number,
    required: true,
    default:'00000000'
  },
  street: {
    type: String,
    required: true,
    default: 'Street '
  },
  city: {
    type: String,
    required: true,
    default: 'Maryville'
  },
  state: {
    type: String,
    required: true,
    default: 'MO'
  },
  zip: {
    type: String,
    required: true,
    default: '64468'
  },
  country: {
    type: String,
    required: true,
    default: 'USA'
  }
 


})
module.exports = mongoose.model('Customer', CustomerSchema)