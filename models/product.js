/** 
*  Customer model
*  Describes the characteristics of each attribute in a product resource.
*
* @author Vennela Cheekoti <S533619@NWMISSOURI.EDU>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  product_Type: {
    type: String,
    required: true
  }, product_Name: {
    type: String,
    required: true
  }, 
  product_Description: {
    type: String,
    required: false
  },
  product_Price: { 
    type: Number,
    required: true,
    min: 1,
    max: 5000
  },
  seller_ID:{
      type: Number,
      required: true
  }
})
module.exports = mongoose.model('Product', ProductSchema)
